data "sbercloud_images_image" "myimage" {
  name        = "Ubuntu 20.04 server 64bit"
  most_recent = true
}

resource "sbercloud_compute_instance" "ecs_m" {
  name              = "upp-master"      
  image_id          = data.sbercloud_images_image.myimage.id
  flavor_id         = "s6.large.2"
  key_pair = "KeyPair-Rudometov-personal"
  security_groups   = ["upp-sg"]
  availability_zone = "ru-moscow-1a"
   
  system_disk_type = "SAS"
  system_disk_size = 20 

  network {
    uuid = sbercloud_vpc_subnet.subnet_1.id
  }
}  

resource "sbercloud_compute_instance" "ecs_1" {
  name              = "upp-worker-one"      
  image_id          = data.sbercloud_images_image.myimage.id
  flavor_id         = "s6.large.2"
  key_pair = "KeyPair-Rudometov-personal"
  security_groups   = ["upp-sg"]
  availability_zone = "ru-moscow-1a"
   
  system_disk_type = "SAS"
  system_disk_size = 20
 
  network {
    uuid = sbercloud_vpc_subnet.subnet_1.id
  }
}  