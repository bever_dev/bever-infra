# Алгоритм развертывания инфраструктуры

## Prereq:
- kubectl
- helm
- k0ctl

1. Развернуть виртуалки: 
    -   <details>
        <summary> Задать переменные среды</summary>

        ```
        - SBC_REGION_NAME
        - SBC_ACCESS_KEY
        - SBC_SECRET_KEY
        - SBC_PROJECT_NAME
        ```
        </details>
        
    - `cd terraform && terraform apply`
    - выяснить EIP кластера и IP нод
2. Развернуть `k8s`
    -   <details>
        <summary> Задать переменные среды</summary>
        
        ```
        - K0C_EIP  # cluster eip
        - K0C_W1IP # ip of upp-worker-one
        - K0C_W2IP # ip of upp-worker-one
        - K0C_KEY  # path to key for ssh
        ```
        </details>
    - `$ k0sctl apply --config manifests/k0s-config.yaml`
    - `$ k0sctl kubeconfig --config manifests/k0s-config.yaml > ~/.kube/config`
3. Развернуть `nginx` и `certmanager`
    - следуй [скрипту](scripts/ingress.md)
4. Развернуть `sonar`
    - следуй [скрипту](scripts/sonar.md)
5. Развернуть `jenkins`
    - следуй [скрипту](scripts/jenkins.md)
6. Развернуть `postgres` и нагрузку
    - следуй [скрипту](scripts/load.md)

### Notes

Для изучения чартов удобны

    helm pull <repo>/<chart>
    helm upgrade --install --dry-run <...> > generated-manifests.yaml