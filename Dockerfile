FROM maven:3.8.4-jdk-11-slim AS MAVEN_BUILD

COPY pom.xml /build/
COPY src /build/src/

WORKDIR /build/

RUN mvn -Dmaven.test.skip=true package

FROM openjdk:11-jre

WORKDIR /app

COPY --from=MAVEN_BUILD /build/target/registration-0.0.1-SNAPSHOT.jar /app/

ENTRYPOINT ["java", "-jar", "registration-0.0.1-SNAPSHOT.jar", "--spring.config.location=/opt/application.properties"]
