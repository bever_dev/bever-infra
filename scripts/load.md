### Postgres

    kubectl create -f manifests/postgres/pv-postgres.yaml
    helm repo add bitnami https://charts.bitnami.com/bitnami
    helm update
    helm install postgres bitnami/postgresql -f manifests/postgres/values.yaml

### Useful helm output

```
PostgreSQL can be accessed via port 5432 on the following DNS names from within your cluster:

    postgres-postgresql.default.svc.cluster.local - Read/Write connection

To get the password for "postgres" run:

    export POSTGRES_PASSWORD=$(kubectl get secret --namespace default postgres-postgresql -o jsonpath="{.data.postgres-password}" | base64 --decode)

To connect to your database run the following command:

    kubectl run postgres-postgresql-client --rm --tty -i --restart='Never' --namespace default --image docker.io/bitnami/postgresql:14.2.0-debian-10-r77 --env="PGPASSWORD=$POSTGRES_PASSWORD" \
      --command -- psql --host postgres-postgresql -U postgres -d postgres -p 5432

    > NOTE: If you access the container using bash, make sure that you execute "/opt/bitnami/scripts/entrypoint.sh /bin/bash" in order to avoid the error "psql: local user with ID 1001} does not exist"

To connect to your database from outside the cluster execute the following commands:

    kubectl port-forward --namespace default svc/postgres-postgresql 5432:5432 &
    PGPASSWORD="$POSTGRES_PASSWORD" psql --host 127.0.0.1 -U postgres -d postgres -p 5432
```

databases used: 

    CREATE DATABASE bever_auth;
    CREATE DATABASE bever_lib;
    CREATE DATABASE bever_img;
    CREATE DATABASE bever_rev;
    CREATE DATABASE bever_pref;
### Auth service

for testing:

    MOUNT_PATH=/home/andy/Code/2022/upp/back_end/build/resources/
    docker run -it  --name=test-auth --mount src="$(MOUNT_PATH)",target=/opt/,type=bind  ultimatehikari/bever-auth:0.2.1 -c "sleep 10000 &"
    docker run -it  --name=test-auth --mount src=/home/andy/Code/2022/upp/back_end/build/resources/,target=/opt/,type=bind  ultimatehikari/bever-auth:0.2.1


build image

    ./gradlew bootJar
    docker build . -t ultimatehikari/bever-auth:<tag>
    docker push ultimatehikari/bever-auth:<tag>

then create configmap from application.yml, replacing password with actual

    kubectl create configmap bever-auth-config --from-file=load/reg-auth/application.yml

then deploy

    kubectl create -f load/reg-auth/dp_reg-auth.yaml

do not forget

    kubectl apply -f load/ingress.yaml

### proto-library

build image

    ./gradlew build
    docker build . -t ultimatehikari/bever-lib:<tag>
    docker push ultimatehikari/bever-lib:<tag>

then create configmap from configs, replacing password with actual in **BOTH**

    kubectl create configmap bever-lib-config --from-file=load/lib/application.properties --from-file=load/lib/cfg.xml

then deploy

    kubectl create -f load/lib/dp_lib.yaml

do not forget to update ingress

    kubectl apply -f load/ingress.yaml

### front

build image

    cd front_end
    docker build . -t ultimatehikari/bever-front:<tag>
    docker push ultimatehikari/bever-front:<tag>

deploy

    kubectl create -f load/front/dp_front.yaml

do not forget to update ingress

    kubectl apply -f load/front/ingress.yaml

### imageserver

container  ultimatehikari/bever-coderkan-img:1.0 is based on https://github.com/coderkan/imageserver,  
last commit from 10.01.2021  

image should be prebuilt, in case of emergency  
(also you need to bump lombok version in pom at least to 1.18.22)

    git clone https://github.com/coderkan/imageserver
    cp load/imageserver/Dockerfile imageserver/
    cd imageserver
    mvn clean package spring-boot:repackage -DskipTests
    docker build . -t ultimatehikari/bever-coderkan-img:1.0
    docker push ultimatehikari/bever-coderkan-img:1.0

then create configmap from application.yml, replacing password with actual

    kubectl create configmap bever-img-config --from-file=load/imageserver/application.yml

then deploy

    kubectl create -f load/imageserver/dp_image.yaml

do not forget to update ingress

    kubectl apply -f load/ingress.yaml

### review service

build image

    cd back_end/reviewService
    ./gradlew build
    docker build . -t ultimatehikari/bever-rev:<tag>
    docker push ultimatehikari/bever-rev:<tag>

then create configmap from configs, replacing password with actual

    kubectl create configmap bever-rev-config --from-file=load/rev/application.properties

then deploy

    kubectl create -f load/rev/dp_rev.yaml

do not forget to update ingress

    kubectl apply -f load/ingress.yaml

### preference service

build image

    cd back_end/prefer
    ./gradlew build
    docker build . -t ultimatehikari/bever-pref:<tag>
    docker push ultimatehikari/bever-pref:<tag>

then create configmap from configs, replacing password with actual

    kubectl create configmap bever-pref-config --from-file=load/pref/application.yml

then deploy

    kubectl create -f load/pref/dp_pref.yaml

do not forget to update ingress

    kubectl apply -f load/ingress.yaml