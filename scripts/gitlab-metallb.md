```
$ helm repo add gitlab https://charts.gitlab.io/
$ helm repo update
$ helm upgrade --install gitlab gitlab/gitlab \
  --timeout 600s \
  --set global.hosts.domain=bever.fun \
  --set global.hosts.externalIP=46.243.142.242 \
  --set certmanager-issuer.email=a.rudometov@g.nsu.ru
```

```
helm repo add metallb https://metallb.github.io/metallb
helm install metallb metallb/metallb -f values.yaml
```

```
configInline:
  address-pools:
   - name: default
     protocol: layer2
     addresses:
     - 	46.243.142.242/32
```
