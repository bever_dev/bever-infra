[https://www.jenkins.io/doc/book/installing/kubernetes/#install-jenkins]
### prereq
ensure you have nginx ingress from gitlab installation;  
also better deploy `manifests/jenkins/jenkins-issuer.yaml` first.
### prepare
```
helm repo add jenkinsci https://charts.jenkins.io
helm repo update
```
add pv & service account:
```
kubectl create namespace jenkins
kubectl apply -f manifests/jenkins/pv-jenkins.yaml
kubectl apply -f manifests/jenkins/jenkins-sa.yaml
```
prepare values, example in manifests/jenkins-values.yaml
then install stuff
```
helm upgrade --install jenkins -n jenkins jenkinsci/jenkins -f manifests/jenkins/jenkins-values.yaml
```

changes in values:
- hostname
- ingress
- removed dep on conf-as-code, is taken from k8s dep anyways

do not forget to give write right with chmod to pv, located in `/data`
### setup login
- tick Manage Jenkins/Security/Enable proxy compatibility
- tick legacy mode

### login
```
jsonpath="{.data.jenkins-admin-password}"
secret=$(kubectl get secret -n jenkins jenkins -o jsonpath=$jsonpath)
echo $(echo $secret | base64 --decode)
```
create ingress:
```
kubectl create -f manifests/jenkins/jenkins-ingress.yaml -n jenkins
```
### pipeline setup
- generate token on sonar server
- generate token on gitlab
- install gitlab plugin on jenkins
- install sonar-scanner plugin on jenkins
    - in config do not forget additional
    - -Dsonar.projectKey=bever -Dsonar.organization=bever-group
    - also https everywhere or shitty java parser gets mad
- install kubernetes-cli plugin

- add credentials in jenkins as
    - sonar token
    - gitlab token
    - gitlab ssh key for SCM
    - docker creds
    - kubeconfig
- setup pod agent with dind pod
    - should run in privilieged mode
    - you must remove sleep 999999

- install openjdk17 with adopt plugin
    - dind has only 11

- create new pipeline with jenkinsfile
    - set to SCM
    - and to trigger on gitlab thingy
