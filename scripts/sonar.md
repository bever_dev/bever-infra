### Installation

    kubectl create -f manifests/sonarqube/pv-postgres.yaml
    kubectl create -f manifests/sonarqube/sonar-issuer.yaml -n sonarqube

    helm repo add sonarqube https://SonarSource.github.io/helm-chart-sonarqube
    helm repo update
    kubectl create namespace sonarqube
    helm upgrade --install -n sonarqube sonarqube sonarqube/sonarqube \
        -f manifests/sonarqube/values.yaml

My changes in default values:
- less memory for pvc, 20->10Gi
- enabled Ingress creation
- added toleration for taint
- persistence.enabled=false

### Optional stuff

    kubectl taint nodes upp-worker-one sonarqube=true:NoSchedule
