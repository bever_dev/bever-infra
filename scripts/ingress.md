### Nginx
For deploy:

    kubectl create namespace ingress-nginx
    kubectl apply -f manifests/nginx/nginx-cm.yaml
    kubectl apply -f manifests/nginx/deploy.yaml

For tests: /testpath should spit out different 404 html:
    kubectl apply -f manifests/nginx/ingress.yaml

### Cert-manager
For deploy:

    helm repo add jetstack https://charts.jetstack.io
    helm repo update
    kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.8.0/cert-manager.crds.yaml
    helm install \
    cert-manager jetstack/cert-manager \
    --namespace cert-manager \
    --create-namespace \
    --version v1.8.0