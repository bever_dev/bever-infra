### Prereq:
* install `kubectl`
* install `helm`
* `scp` cert from node and fix ip to eip
* run `k get pods` check

### Before:
go into `manifests/gitlab`and `k apply -f` all yamls

### Helm stuff:

```
helm repo add gitlab https://charts.gitlab.io/
helm repo update
helm upgrade --install gitlab gitlab/gitlab -n gitlab \
  --timeout 300s \
  --set global.hosts.domain=bever.fun                   \
  --set global.hosts.externalIP=37.18.110.121           \
  --set certmanager-issuer.email=a.rudometov@g.nsu.ru   \
  --set tcp.22="gitlab/mygitlab-gitlab-shell:22"        \
  --set global.ingress.class=nginx                      \
  --set nginx-ingress.enabled=false                     \
  --set gitlab.gitaly.persistence.size=5Gi              \
  --set minio.persistence.size=5Gi               \
  --set postgresql.persistence.size=5Gi             \
  --set prometheus.server.persistentVolume.size=5Gi          \
  --set redis.master.persistence.size=5Gi
```

### Note:
On helm uninstall **do not forget to** 
```
k delete pvc --all -n gitlab
k delete pv --all
```

### then:

go into `/mnt/data/` and give everyone `chmod g+w <dir>`

### initial login

```
kubectl get secret -n gitlab gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo
```
### omniauth:
1. get values

    helm show values gitlab/gitlab > values.yaml

2. Change omniauth config in values.yaml. Config example:

    omniauth:
    enabled: true # Default: false
    autoSignInWithProvider:
    syncProfileFromProvider: [ ]
    syncProfileAttributes: [ 'email' ]
    allowSingleSignOn: [ 'saml', 'google_oauth2' ] # Default: ['saml']
    blockAutoCreatedUsers: false # Default: true
    autoLinkLdapUser: true # Default: false
    autoLinkSamlUser: false
    autoLinkUser: [ ]
    externalProviders: [ ]
    allowBypassTwoFactor: [ ]
    providers: # Default: []
        - secret: gitlab-google-oauth2
3. 
    k create secret generic -n gitlab gitlab-google-oauth2 --from-file=provider=manifests/google-provider-secret.yaml  
4.
    helm upgrade --install gitlab gitlab/gitlab -n gitlab \
    --timeout 300s \
    --set global.hosts.domain=bever.fun                 \
    --set global.hosts.externalIP=37.18.110.121         \
    --set certmanager-issuer.email=a.rudometov@g.nsu.ru \
    --set tcp.22="gitlab/mygitlab-gitlab-shell:22"      \
    --set global.ingress.class=nginx                    \
    --set nginx-ingress.enabled=false                   \
    --set gitlab.gitaly.persistence.size=5Gi            \
    --set minio.persistence.size=5Gi                    \
    --set postgresql.persistence.size=5Gi               \
    --set prometheus.server.persistentVolume.size=5Gi   \
    --set redis.master.persistence.size=5Gi             \
    -f manifests/gitlab/gitlab-values.yaml \
	--version=5.8.2
